import qualified Data.Map as M
import System.Exit ( exitSuccess )
import XMonad
import qualified XMonad.StackSet as W
import XMonad.Hooks.DynamicLog ( PP (ppLayout, ppSort, ppTitle
   , ppTitleSanitize, ppVisible), statusBar, wrap )
import XMonad.Hooks.EwmhDesktops ( fullscreenEventHook )
import XMonad.Hooks.ManageDocks
   ( ToggleStruts (..), avoidStruts, manageDocks )
import XMonad.Hooks.ManageHelpers
   ( (-?>), composeOne, isDialog, transience )
import XMonad.Layout.MultiColumns ( multiCol )
import XMonad.Layout.Tabbed ( simpleTabbed )
import XMonad.Layout.ThreeColumns ( ThreeCol (ThreeCol, ThreeColMid) )
import XMonad.Prompt ( XPPosition (Top), alwaysHighlight, font
   , position, promptBorderWidth )
import XMonad.Prompt.ConfirmPrompt ( confirmPrompt )
import XMonad.Prompt.Shell ( shellPrompt )
import XMonad.Util.WorkspaceCompare ( getSortByXineramaRule )
import XMonad.Hooks.DynamicLog
import XMonad.Util.EZConfig(additionalKeys)
import System.IO
import Graphics.X11.ExtraTypes.XF86
import XMonad.Actions.CycleWindows
import XMonad.Actions.RotSlaves
import XMonad.Layout.WindowNavigation
import XMonad.Actions.CycleRecentWS
import XMonad.Actions.CycleWS

myTerminal      = "terminator"

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
--
myBorderWidth   = 2

myModMask       = mod4Mask  -- The Windows logo key

myWorkspaces    = ["WEB","TERM","FILES","EDITOR","SOCIAL","SERVERS"]

myNormalBorderColor  = "#000000"
myFocusedBorderColor = "#00ff00"  -- green

--------------------------------------------------------------------------------
-- | Customize the way 'XMonad.Prompt' looks and behaves.  It's a
-- great replacement for dzen.
myXPConfig = def
  { position          = Top
  , alwaysHighlight   = True
  , promptBorderWidth = 0
  , font              = "xft:ApercuMonoPro:size=12"
  }

------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- launch a terminal
    [ ((modm,               xK_Return), spawn $ XMonad.terminal conf)

    -- launch dmenu
    --, ((modm,               xK_d     ), spawn "dmenu_run -fn 'Apercu Mono Pro-16' -nb '#fdf6e3' -nf '#586c75' -sf '#fdf6e3' -sb '#b58900'")
    , ((modm,               xK_d     ), spawn "rofi -show run -modi run -location 1 -width 100 -lines 10 -line-margin 1 -line-padding 1 -separator-style none -font 'ApercuMonoPro 14' -columns 10 -bw 1  -disable-history -hide-scrollbar -color-window '#222222, #222222, #b1b4b3' -color-normal '#222222, #b1b4b3, #222222, #005577, #b1b4b3' -color-active '#222222, #b1b4b3, #222222, #007763, #b1b4b3' -color-urgent '#222222, #b1b4b3, #222222, #77003d, #b1b4b3' -kb-row-select 'Tab' -kb-row-tab ''")

    -- close focused window
    , ((modm .|. shiftMask, xK_q     ), kill)

     -- Rotate through the available layout algorithms
    , ((modm,               xK_space ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    , ((modm,               xK_Tab   ), windows W.focusDown)

    -- Move focus to the next window
    , ((modm,               xK_j     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm .|. shiftMask, xK_Tab ), windows W.focusUp  )

    -- Move focus to the previous window
    , ((modm,               xK_k     ), windows W.focusUp  )

    -- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm .|. shiftMask, xK_Return), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_l     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --
    , ((modm              , xK_b     ), sendMessage ToggleStruts)

    -- Quit xmonad
    ---, ((modm .|. shiftMask, xK_c     ), io (exitWith ExitSuccess))
    
       --take a screenshot of entire display
   , ((controlMask, xK_Print ), spawn "maim | xclip -selection clipboard -t image/png ~/Pictures/$(date +%s).png")

   --take a screenshot of focused window
   --- , ((shiftMask, xK_Print ), spawn "maim -s | xclip -selection clipboard -t image/png ~/Pictures/$(date +%s).png")

   , ((shiftMask, xK_Print ), spawn "maim -s /tmp/screenshot.png; imgur.sh /tmp/screenshot.png | xclip -selection clipboard && sleep 20 && xclip -selection clipboard -o >> ~/Pictures/imgur_log/screenshot_log.txt && imgur_copy")

    -- Restart xmonad
    , ((modm   .|. shiftMask, xK_r     ), spawn "xmonad --recompile; xmonad --restart")

    -- Run xmessage with a summary of the default keybindings (useful for beginners)
    , ((modm .|. shiftMask, xK_slash ), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))

    -- Quit xmonad
    --, ((modm .|. shiftMask, xK_c     ), confirmPrompt myXPConfig "exit" (io exitSuccess))

    -- launch xscreensaver-command -lock
    , ((modm .|. shiftMask, xK_l     ), spawn "xscreensaver-command -lock")

    -- launch xscreensaver-command -lock AND power the monitor down
    , ((modm .|. controlMask, xK_l   ), spawn "xscreensaver-command -lock; sleep 1; xset dpms force off")

    -- cycling through windows
    , ((modm,               xK_Down),  nextWS)
    , ((modm,               xK_Up),    prevWS)
    , ((modm .|. shiftMask, xK_Down),  shiftToNext)
    , ((modm .|. shiftMask, xK_Up),    shiftToPrev)
    , ((modm,               xK_Right), nextScreen)
    , ((modm,               xK_Left),  prevScreen)
    , ((modm .|. shiftMask, xK_Right), shiftNextScreen)
    , ((modm .|. shiftMask, xK_Left),  shiftPrevScreen)
    , ((modm,               xK_z),     toggleWS)
    , ((modm .|. shiftMask, xK_Down), shiftToNext >> nextWS)
    , ((modm .|. shiftMask, xK_Up),   shiftToPrev >> prevWS)

    ]
    ++

    --
    -- switch workspaces
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_6]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Layouts:

-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--
myLayout = avoidStruts $
   simpleTabbed |||
   tiled |||
   Full

   where
      -- default tiling algorithm partitions the screen into two panes
      tiled   = Tall nmaster delta ratio

      -- The default number of windows in the master pane
      nmaster = 1

      -- Default proportion of screen occupied by master pane
      ratio   = 1/2

      -- Percent of screen to increment by when resizing panes
      delta   = 3/100

------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = manageDocks <+> composeOne

  [ className =? "Gimp"   -?> doFloat

  -- This turned out to be annoying
  -- , isDialog -?> doCenterFloat
  , isDialog -?> doFloat

  -- Move transient windows to their parent:
  , transience
  ]

------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
--myEventHook = mempty
--myEventHook = fullscreenEventHook
myEventHook = handleEventHook def <+> fullscreenEventHook

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
myLogHook = return ()
-- This one came from defaultDesktop config example:
--myLogHook = dynamicLogString def >>= xmonadPropLog

------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = return ()

------------------------------------------------------------------------
-- My custom stdin pretty-printer for xmobar. Only interested in
-- workspaces at this time
myPP = def
   { ppLayout = const ""  -- Don't show the layout name
   , ppTitle = const ""  -- Don't show the focused window's title
   , ppTitleSanitize = const ""  -- Also about window's title
   , ppVisible = wrap "(" ")"  -- Non-focused (but still visible) screen
   }

------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
main = xmonad =<< statusBar "xmobar" myPP toggleStrutsKey defaults
toggleStrutsKey XConfig { XMonad.modMask = modMask } = (modMask, xK_b)

-- A structure containing your configuration settings, overriding
-- fields in the default config. Any you don't override, will
-- use the defaults defined in xmonad/XMonad/Config.hs
--
-- No need to modify this.
--
defaults = def {
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        logHook            = myLogHook,
        startupHook        = myStartupHook
    }
    `additionalKeys`
    [

    -- volume control
      ((0 , xF86XK_AudioRaiseVolume), spawn "pactl set-sink-volume @DEFAULT_SINK@ +2%; exec pactl set-sink-mute @DEFAULT_SINK@ 0")
    , ((0 , xF86XK_AudioLowerVolume), spawn "pactl set-sink-volume @DEFAULT_SINK@ -2%; exec pactl set-sink-mute @DEFAULT_SINK@ 0")
    , ((0 , xF86XK_AudioMute), spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
    , ((mod4Mask .|. controlMask, xK_Up   ), spawn "amixer -D pulse sset Master 10%+")
    , ((mod4Mask .|. controlMask, xK_Down ), spawn "amixer -D pulse sset Master 10%-")


    ]

-- | They keybindings for this config file, accessible via mod-Shift-/
help :: String
help = unlines
   [ "XMonad keybindings help"
   , ""
   , "The modifier key is 'super'. Keybindings:"
   , ""
   , "-- launching and killing programs"
   , "mod-Enter  Launch " ++ myTerminal
   , "mod-d            dmenu"
   , "mod-Shift-q      Close/kill the focused window"
   , "mod-Space        Rotate through the available layout algorithms"
   , "mod-Shift-Space  Reset the layouts on the current workSpace to default"
   , "mod-n            Resize/refresh viewed windows to the correct size"
   , "mod-Shift-l      Lock workstation with xscreensaver"
   , ""
   , "-- move focus up or down the window stack"
   , "mod-Tab        Move focus to the next window"
   , "mod-Shift-Tab  Move focus to the previous window"
   , "mod-j          Move focus to the next window"
   , "mod-k          Move focus to the previous window"
   , "mod-m          Move focus to the master window"
   , ""
   , "-- modifying the window order"
   , "mod-Shift-Return   Swap the focused window and the master window"
   , "mod-Shift-j  Swap the focused window with the next window"
   , "mod-Shift-k  Swap the focused window with the previous window"
   , ""
   , "-- resizing the master/slave ratio"
   , "mod-h  Shrink the master area"
   , "mod-l  Expand the master area"
   , ""
   , "-- floating layer support"
   , "mod-t  Push window back into tiling; unfloat and re-tile it"
   , ""
   , "-- increase or decrease number of windows in the master area"
   , "mod-comma  (mod-,)  Increment the number of windows in the master area"
   , "mod-period (mod-.)  Deincrement the number of windows in the master area"
   , ""
   , "-- quit, or restart"
   , "mod-Shift-c  Quit xmonad"
   , "mod-Shift-r  Restart xmonad"
   , ""
   , "-- Workspaces & screens"
   , "mod-[1..9]         Switch to workSpace N"
   , "mod-Shift-[1..9]   Move client to workspace N"
   , ""
   , "-- Mouse bindings: default actions bound to mouse events"
   , "mod-button1  Set the window to floating mode and move by dragging"
   , "mod-button2  Raise the window to the top of the stack"
   , "mod-button3  Set the window to floating mode and resize by dragging"
   , ""
   , "-- Miscellaneous bindings"
   , "mod-b                Toggle the status bar gap"
   , "mod-Shift-/ (mod-?)  Show this help dialog"
   ]
